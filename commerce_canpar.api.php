<?php
/**
 * @file
 * Hooks provided by the Commerce Canpar module.
 */

/**
 * Allows modules to alter the shipping rate request before it is submitted.
 *
 * @param array &$querystring
 *   An array containing the shipping rate request (order data that
 *   has been formatted for submission to the Canpar server).
 * @param object $order
 *   The order object the shipping rate request is being generated for.
 *
 * @see commerce_canpar_build_request()
 */
function hook_commerce_canpar_build_rate_request_alter(array &$querystring, $order) {
  // EXAMPLE: Set shipping address.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $shipping_field_name = commerce_physical_order_shipping_field_name($order);
  if ($shipping_field_name) {
    if (!empty($order_wrapper->{$shipping_field_name}->commerce_customer_address)) {
      $shipping_address = $order_wrapper->{$shipping_field_name}->commerce_customer_address->value();
      if ($shipping_address) {
        $querystring['dest'] = $shipping_address;
      }
    }
  }
}
