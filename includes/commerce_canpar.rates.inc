<?php

/**
 * @file
 * Rate request/response functions for the Commerce Canpar module.
 */

/**
 * Builds the query string that will be sent to Canpar.
 *
 * @param array $shipping_method
 *   The shipping method that is used for request.
 * @param object $order
 *   The order to generate a rate request for.
 *
 * @return sting
 *   A query string.
 */
function commerce_canpar_build_request(array $shipping_method, $order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Determine the shipping profile reference field name for the order.
  $shipping_field_name = commerce_physical_order_shipping_field_name($order);

  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->{$shipping_field_name}->commerce_customer_address)) {
    $shipping_address = $order_wrapper->{$shipping_field_name}->commerce_customer_address->value();
  }
  else {
    $shipping_address = array(
      'locality' => '',
      'administrative_area' => '',
      'postal_code' => '',
      'country' => '',
    );
  }

  // Add products into single package.
  $package = commerce_canpar_package_products($order);

  // Use customer's language.
  $language = 'en';
  if (module_exists('i18n') && (i18n_get_lang() == 'fr')) {
    $language = 'fr';
  }

  $service = commerce_canpar_commerce_shipping_service_name($shipping_method['slug']);

  // Add insurance if requested.
  $insurance = variable_get('commerce_canpar_insurance', FALSE) ? $package->price : 0;

  // Get postal codes and trim all whitespaces.
  $origin_postal_code = str_replace(' ', '', variable_get('commerce_canpar_postalcode', 0));
  $destination_postal_code = str_replace(' ', '', $shipping_address['postal_code']);

  // Create an array of rate request data.
  if (variable_get('commerce_canpar_quote_type', 'base') == 'custom') {
    $query = array(
      'shipment' => array(
        'language' => $language,
        'weight_system' => 'METRIC',
        'shipper_number' => variable_get('commerce_canpar_shipper_number', 0),
        'origin_postal_code' => $origin_postal_code,
        'destination_postal_code' => $destination_postal_code,
        'service_type' => $service,
      ),
      'total' => array(
        'total_pieces' => 1,
        'total_weight' => $package->weight['weight'],
        'total_cod' => 0,
        'total_dv' => commerce_currency_amount_to_decimal($insurance, 'CAD'),
        'total_xc' => 0,
      ),
    );
    $shipment = new SimpleXMLElement('<shipment/>');
    foreach ($query['shipment'] as $key => $value) {
      $shipment->addAttribute($key, $value);
    }

    $shipment->addChild('total');
    foreach ($query['total'] as $key => $value) {
      $shipment->total->addAttribute($key, $value);
    }

    $shipment_xml = $shipment->asXML();
    $shipment_xml = substr($shipment_xml, strpos($shipment_xml, '?' . '>') + 2);

    $querystring = array(
      'shipment' => $shipment_xml,
      'token' => variable_get('commerce_canpar_user_token', ''),
    );

  }
  else {
    $querystring = array(
      'service' => $service,
      'quantity' => 1,
      'origin' => $origin_postal_code,
      'dest' => $destination_postal_code,
      'weight' => $package->weight['weight'],
      'unit' => 'K',
      'cod' => 0,
      'dec' => commerce_currency_amount_to_decimal($insurance, 'CAD'),
      'put' => 0,
      'xc' => 0,
    );
  }

  // Log the API request if specified.
  $logging = variable_get('commerce_canpar_log', array());
  if (isset($logging['request']) && $logging['request']) {
    watchdog('commerce_canpar', 'Submitting API request to the Canpar: <pre>%query</pre>', array('%query' => print_r($querystring, TRUE)), WATCHDOG_NOTICE);
  }

  // Allow modules an opportunity to alter the query before it is sent.
  drupal_alter('commerce_canpar_build_request', $querystring, $order);

  return $querystring;
}

/**
 * Sends quote request to Canpar server, returns object with results.
 */
/**
 * Submits a request to the Canpar API.
 *
 * @param string $url
 *   URL to the Canpar API.
 * @param array $query
 *   Parameters that should be used for query in the request.
 *
 * @return SimpleXMLElement
 *   Returns a SimpleXML Element containing the response from the API server or
 *   FALSE if there was no response or an error occurred.
 */
function commerce_canpar_send_request($url, array $query, $message = '') {
  $querystring = drupal_http_build_query($query);
  $response = drupal_http_request($url . '?' . $querystring, array('method' => 'GET'));

  try {
    $xml = new SimpleXMLElement($response->data);
  }
  catch (Exception $e) {
    watchdog('commerce_canpar', 'Shipping failed. Canpar API request failed: @error', array('%error' => print_r($response->data, TRUE)), WATCHDOG_ERROR);
    return FALSE;
  }

  // Log the API response if specified.
  $logging = variable_get('commerce_canpar_log', array());
  if (isset($logging['response']) && $logging['response']) {
    watchdog('commerce_canpar', 'API response received: <pre>%xml</pre>', array('%xml' => print_r($xml, TRUE)), WATCHDOG_NOTICE);
  }

  return $xml;
}

/**
 * Parse a valid XML response into a commerce_shipping rates array.
 *
 * @param array $shipping_method
 *   The shipping method that is used for request.
 * @param SimpleXMLElement $response
 *   A SimpleXML Element containing the response from the API server.
 * @param string $quote_type
 *   The type of shipping quote that is currently used.
 *
 * @return array
 *   A commerce_shipping rates array indexed by shipping service or an empty
 *   array if there was a problem parsing the data.
 */
function commerce_canpar_parse_rate_response(array $shipping_method, SimpleXMLElement $response, $quote_type) {
  $rates = array();

  $data = array(
    'product_name' => (string) $shipping_method['title'],
    'product_id'   => $shipping_method['slug'],
  );

  if ($quote_type == 'custom') {
    $custom_rate = (array) $response->rate[0];
    $total_rates = (float) $custom_rate['@attributes']['total_charge'];
    $amount = commerce_canpar_rate_markup($total_rates);
    $data['delivery_date'] = (string) $custom_rate['@attributes']['estimated_delivery_date'];
  }
  else {
    $total_rates = (array) $response->CanparCharges;
    $amount = commerce_canpar_rate_markup(array_sum($total_rates));
    $data['shipping_date'] = (string) $response->CanparShipment->ShippingDate;
    $data['delivery_date'] = (string) $response->EstimatedDeliveryDate;
  }

  $currency_code = 'CAD';

  if (!$amount) {
    return FALSE;
  }

  // Add an item to the rates array for the current service.
  $rates = array(
    'amount' => commerce_currency_decimal_to_amount($amount, $currency_code),
    'currency_code' => $currency_code,
    'data' => $data,
  );

  return $rates;
}


/**
 * Builds array of package data for rate request.
 *
 * @param object $order
 *   The order object to build the package data from.
 *
 * @return array
 *   An associative array containing a list of packages and their attributes
 *   (weight, dimensions, description, etc.)
 */
function commerce_canpar_package_products($order) {

  // Create a package.
  $package  = new stdClass();
  $package->quantity     = 0;
  $package->price        = 0.0;
  $package->weight       = 0.0;
  $package->weight_units = 'KG';

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($wrapper->commerce_line_items as $line_item_wrapper) {

    // Get product line items from order.
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
      $line_item_weight = commerce_physical_product_line_item_weight($line_item_wrapper->value());
      $item_weight = $line_item_weight ? physical_weight_convert($line_item_weight, 'kg') : 0;
      $item_price  = $line_item_wrapper->commerce_unit_price->amount->value();
      $quantity    = $line_item_wrapper->quantity->value();

      $package->quantity += $quantity;
      $package->price    += $item_price;
      $package->weight   += $item_weight['weight'];
    }
  }

  // Get the total order weight and the weight markup (in kg).
  $order_weight = commerce_physical_order_weight($order, 'kg');
  $package->weight = $order_weight;

  // Markup weight on a per-package basis.
  $package->shipweight = commerce_canpar_weight_markup($package->weight);

  return $package;
}

/**
 * Modifies the rate received from Canpar before displaying to the customer.
 *
 * Rates are marked-up by a percentage, a multiplier, or an additional
 * amount as per the settings in this modules administration menu.
 *
 * @return float
 *   A float containing the modified rate.
 */
function commerce_canpar_rate_markup($rate) {
  $markup = trim(variable_get('commerce_canpar_rate_markup', '0'));
  $type   = variable_get('commerce_canpar_rate_markup_type', 'percentage');
  if (is_numeric($markup)) {
    switch ($type) {
      case 'percentage':
        return (float) $rate * (1.0 + floatval($markup) / 100.0);

      case 'multiplier':
        return (float) $rate * floatval($markup);

      case 'currency':
        return (float) $rate + floatval($markup);
    }
  }
  else {
    return $rate;
  }
}


/**
 * Modifies the weight of shipment before sending to Canpar for a quote.
 *
 * @param int $weight
 *   Shipping weight without any weight markup.
 *
 * @return float
 *   Shipping weight after markup.
 */
function commerce_canpar_weight_markup($weight) {
  $markup = trim(variable_get('commerce_canpar_weight_markup', '0'));
  $type   = variable_get('commerce_canpar_weight_markup_type', 'percentage');

  if (is_numeric($markup)) {
    switch ($type) {
      case 'percentage':
        return (float) $weight * (1.0 + floatval($markup) / 100.0);

      case 'multiplier':
        return (float) $weight * floatval($markup);

      case 'mass':
        return (float) $weight + floatval($markup);
    }
  }
  else {
    return $weight;
  }
}
