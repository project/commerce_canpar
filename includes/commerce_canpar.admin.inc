<?php

/**
 * @file
 * Handles admin settings page for Commerce Canpar module.
 */

/**
 * Default Canpar rate API settings.
 *
 * Configures Canpar URL, available services, and other settings related
 * to shipping quotes.
 *
 * @return array
 *   Forms for store administrator to set configuration options.
 *
 * @see commerce_canpar_settings_form_validate()
 *
 * @ingroup forms
 */
function commerce_canpar_settings_form($form, &$form_state) {

  // Container for credentials forms.
  $form['commerce_canpar_credentials'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Credentials'),
    '#description'   => t('Account number and authorization information.'),
    '#collapsible'   => TRUE,
  );

  // Form to set choose between BASE rates and CUSTOM rates.
  // ***Defaults to BASE!***
  $form['commerce_canpar_credentials']['commerce_canpar_quote_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Canpar quote type'),
    '#description'   => t('Choose to present the customer with Canpar base prices or your custom discounted Canpar account prices. The shipper number and user token fields below are mandatory when custom prices is selected.'),
    '#options'       => array(
      'base'    => t('Base prices'),
      'custom' => t('Discount custom prices'),
    ),
    '#default_value' => variable_get('commerce_canpar_quote_type', 'base'),
  );

  // Form to set the shipper number.
  $form['commerce_canpar_credentials']['commerce_canpar_shipper_number'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Canpar shipper number'),
    '#default_value' => variable_get('commerce_canpar_shipper_number', 99999999),
    '#required'      => FALSE,
    '#description'   => t('Your Canpar shipper number. Required if you have selected custom prices above.'),
  );

  // Form to set the user token.
  $form['commerce_canpar_credentials']['commerce_canpar_user_token'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Canpar user token'),
    '#default_value' => variable_get('commerce_canpar_user_token', 'CANPAR'),
    '#required'      => FALSE,
    '#description'   => t('Your Canpar user token. Required if you have selected custom prices above.'),
  );
  $form['commerce_canpar_credentials']['commerce_canpar_rates_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Cached rates timeout'),
    '#description' => t('Enter the number of seconds after which cached shipping rates should no longer be considered valid.') . '<br />' . t('Set to 0 during testing to avoid caching failed rate request responses.'),
    '#default_value' => variable_get('commerce_canpar_rates_timeout', 60),
    '#field_suffix' => t('seconds'),
    '#size' => 4,
    '#maxlength' => 4,
  );

  $form['commerce_canpar_credentials']['commerce_canpar_log'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => variable_get('commerce_canpar_log', array()),
  );

  // Container for quote options forms.
  $form['commerce_canpar_quote_options'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Quote options'),
    '#description'   => t('Preferences that affect computation of quote.'),
    '#collapsible'   => TRUE,
  );

  // Form to specify turnaround time.
  $form['commerce_canpar_quote_options']['commerce_canpar_turnaround'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Turn-around time'),
    '#default_value' => variable_get('commerce_canpar_turnaround', '24'),
    '#description'   => t('Number of hours for turn-around time before shipping. This simply adds this number of hours to the delivery date as returned by Canpar.'),
    '#size' => 4,
    '#maxlength' => 4,
  );

  $date_format = variable_get('commerce_canpar_delivery_date_format', '');
  if (!in_array($date_format, array_keys(system_get_date_types()))) {
    $date_format = '';
  }
  // Form to specify date format.
  $form['commerce_canpar_quote_options']['commerce_canpar_delivery_date_format'] = array(
    '#type'          => 'radios',
    '#title'         => t('Delivery date format'),
    '#default_value' => $date_format,
    '#description'   => t('Format to display estimated delivery date. See !url to modify available date formats.', array(
      '!url' => l(t('this page'), 'admin/config/regional/date-time'),
    )),
    '#options'       => commerce_canpar_option_list_date_formats(),
  );

  // Form to specify ship-from postal code.
  $form['commerce_canpar_quote_options']['commerce_canpar_postalcode'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Ship from postal code'),
    '#default_value' => variable_get('commerce_canpar_postalcode', ''),
    '#description'   => t('Postal code to ship from.'),
    '#required'      => TRUE,
    '#size' => 7,
    '#maxlength' => 7,
  );

  // Form to add optional Insurance coverage in the amount of the order.
  $form['commerce_canpar_quote_options']['commerce_canpar_insurance'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Add Insurance to shipping quote'),
    '#default_value' => variable_get('commerce_canpar_insurance', FALSE),
    '#description'   => t('When enabled, the quote presented to the customer will include the cost of insurance for the full sales price of all products in the order.'),
  );

  // Container for markup forms.
  $form['commerce_canpar_markups'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Markups'),
    '#description'   => t('Modifiers to the shipping weight and quoted rate.'),
    '#collapsible'   => TRUE,
  );

  // Form to select type of rate markup.
  $form['commerce_canpar_markups']['commerce_canpar_rate_markup_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Rate markup type'),
    '#default_value' => variable_get('commerce_canpar_rate_markup_type', 'percentage'),
    '#options'       => array(
      'percentage' => t('Percentage (%)'),
      'multiplier' => t('Multiplier (×)'),
      'currency'   => t('Addition (!currency)', array('!currency' => '$')),
    ),
  );

  // Form to select rate markup amount.
  $form['commerce_canpar_markups']['commerce_canpar_rate_markup'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Canpar shipping rate markup'),
    '#default_value' => variable_get('commerce_canpar_rate_markup', '0'),
    '#description'   => t('Markup shipping rate quote by dollar amount, percentage, or multiplier. Please note if this field is not blank, it overrides the "Handling fee" set up in your SellOnline account. If blank, the handling fee from your SellOnline account will be used.'),
    '#size' => 4,
    '#maxlength' => 4,
  );

  // Form to select type of weight markup.
  $form['commerce_canpar_markups']['commerce_canpar_weight_markup_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Weight markup type'),
    '#default_value' => variable_get('commerce_canpar_weight_markup_type', 'percentage'),
    '#options'       => array(
      'percentage' => t('Percentage (%)'),
      'multiplier' => t('Multiplier (×)'),
      'mass'       => t('Addition (!mass)', array('!mass' => '#')),
    ),
  );

  // Form to select weight markup amount.
  $form['commerce_canpar_markups']['commerce_canpar_weight_markup'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Canpar shipping weight markup'),
    '#default_value' => variable_get('commerce_canpar_weight_markup', '0'),
    '#description'   => t('Markup Canpar shipping weight before quote, on a per-package basis, by weight amount, percentage, or multiplier.'),
    '#size' => 4,
    '#maxlength' => 4,
  );

  // Container for service selection forms.
  $form['commerce_canpar_service_selection'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Service options'),
    '#collapsible'   => TRUE,
    '#collapsed' => TRUE,
  );

  foreach (_commerce_canpar_service_list() as $key => $service) {
    $array_options[$key] = $service['title'];
  }
  // Form to restrict Canpar services available to customer.
  $form['commerce_canpar_service_selection']['commerce_canpar_services'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Canpar services'),
    '#default_value' => variable_get('commerce_canpar_services', $array_options),
    '#options'       => $array_options,
    '#description'   => t('Select the shipping services that are available to customers.'),
  );

  // Register additional validation handler.
  $form['#validate'][] = 'commerce_canpar_settings_form_validate';

  $form = system_settings_form($form);
  $form['#submit'][] = 'commerce_canpar_settings_form_submit';

  // Add Cancel link.
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/commerce/config/shipping/methods'),
  );

  return $form;
}

/**
 * Validation handler for commerce_canpar_settings_form.
 *
 * Requires shipper number and user token for custom quotes only.
 * Ensures markups are numeric.
 *
 * @see commerce_canpar_settings_form()
 */
function commerce_canpar_settings_form_validate($form, &$form_state) {

  // Canpar shipper number and user token are required for custom quotes,
  // optional otherwise.
  if ($form_state['values']['commerce_canpar_quote_type'] == 'custom') {
    if (trim($form_state['values']['commerce_canpar_shipper_number']) == '') {
      form_set_error(
        'commerce_canpar_shipper_number',
        t('Shipper number field is required when custom quotes are selected.')
      );
    }
    if (trim($form_state['values']['commerce_canpar_user_token']) == '') {
      form_set_error(
        'commerce_canpar_user_token',
        t('User token field is required when custom quotes are selected.')
      );
    }
  }

  // Ensure rate markup is numeric - no % or $ or any other units specified.
  if (!is_numeric($form_state['values']['commerce_canpar_rate_markup'])) {
    form_set_error(
      'commerce_canpar_rate_markup',
      t('Rate markup must be a numeric value.')
    );
  }

  // Ensure weight markup is numeric - no % or lb or any other units specified.
  if (!is_numeric($form_state['values']['commerce_canpar_weight_markup'])) {
    form_set_error(
      'commerce_canpar_weight_markup',
      t('Weight markup must be a numeric value.')
    );
  }

  $form_state['unchanged_canpar_services'] = variable_get('commerce_canpar_services', NULL);
}

/**
 * Submission handler for commerce_canpar_settings_form.
 *
 * If the selected services have changed then rebuild caches..
 *
 * @see commerce_canpar_settings_form()
 */
function commerce_canpar_settings_form_submit($form, &$form_state) {
  if ($form_state['unchanged_canpar_services'] !== $form_state['values']['commerce_canpar_services']) {
    commerce_shipping_services_reset();
    rules_clear_cache(TRUE);
    entity_defaults_rebuild();
    menu_rebuild();
  }
}

/**
 * Returns an option list of date format types.
 *
 * @return array
 *   An associative array keyed by machine name.
 */
function commerce_canpar_option_list_date_formats() {
  $date_options_list = array('' => t('Do not display'));

  foreach (system_get_date_types() as $type) {
    $date_options_list[$type['type']] = t('<strong>!title:</strong> !format', array(
      '!title' => $type['title'],
      '!format' => format_date(REQUEST_TIME, $type['type']),
    ));
  }

  return $date_options_list;
}
